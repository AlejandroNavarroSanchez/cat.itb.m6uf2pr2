import Model.JugadoresEntity;
import ModelCRUD.EstadisticasCRUD;
import ModelCRUD.JugadoresCRUD;
import org.hibernate.Session;

import static Connection.ConnectionNBA.getSession;

public class Main {
    public static void main(final String[] args) {
        final Session session = getSession();

        //EXERCISES
        //Ex 1
//        ex1(session, "05/06", 7.0, 5.0);
//        ex1(session, "06/07", 10.0, 3.0);

        //Ex 2
//        ex2(session, 101, 240);
//        ex2(session, 251, 200);
//        ex2(session, 354, 300);
//        ex2(session, 561, 290);
//        ex2(session, 407, 263);

        //Ex 3
//        ex3(session, 227);
//        ex3(session, 43);
//        ex3(session, 469);

        //Ex 4
//        ex4(session);

        //Ex 5
//        ex5(session);

        session.close();
    }

    /**
     * This method calls EstadisticasCRUD to show the maximum scorer player per season
     * @param session Session
     */
    private static void ex5(Session session) {
        EstadisticasCRUD.maxScorerPerSeason(session);
    }

    /**
     * This method calls JugadoresCRUD to show the average points of every player
     * @param session Session
     */
    private static void ex4(Session session) {
        JugadoresCRUD.showAvgPointsPerPlayer(session);
    }

    /**
     * This method calls JugadoresCRUD to show all info from a player
     * @param session Session
     * @param cod int
     */
    private static void ex3(Session session, int cod) {
        JugadoresEntity player = session.load(JugadoresEntity.class, cod);
        EstadisticasCRUD.showAllPlayerStatistics(session, player);
    }

    /**
     * This method calls JugadoresCRUD to update a player's weight
     * @param session Session
     * @param cod int
     * @param weight int
     */
    private static void ex2(Session session, int cod, int weight) {
        JugadoresCRUD.updateWeight(session, cod, weight);
    }

    /**
     * This method calls EstadisticasCRUD to insert statistics row on player id=123
     * @param session Session
     * @param temporada String
     * @param puntos Double
     * @param rebotes Double
     */
    private static void ex1(Session session, String temporada, Double puntos, Double rebotes) {
        EstadisticasCRUD.insertPlayer123Statistics(session, temporada, puntos, rebotes);
    }
}