package ModelCRUD;

import Model.EstadisticasEntity;
import Model.JugadoresEntity;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.query.Query;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import java.util.List;

import static Connection.ConnectionNBA.getSession;

public class EstadisticasCRUD {
    /**
     * This method shows all information from a player's statistics
     * @param session Session
     * @param playerEntity JugadoresEntity
     */
    public static void showAllPlayerStatistics(Session session, JugadoresEntity playerEntity) {
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<EstadisticasEntity> cq = cb.createQuery(EstadisticasEntity.class);
        Root<EstadisticasEntity> root = cq.from(EstadisticasEntity.class);

        cq.select(root);
        cq.where(cb.equal(root.get("jugadoresByJugador"), playerEntity));

        Query<EstadisticasEntity> query = session.createQuery(cq);
        List<EstadisticasEntity> players = query.getResultList();

        if (players.isEmpty()) {
            System.out.println("El jugador con código " + playerEntity.getCodigo() + " no existe.");
        } else {
            // Mostramos estadísticas del jugador
            JugadoresEntity jugador = players.get(0).getJugadoresByJugador();
            System.out.println("\nDADES DEL JUGADOR: " + jugador.getCodigo());
            System.out.println("Nom: " + jugador.getNombre());
            System.out.println("Equip: " + jugador.getEquiposByNombreEquipo().getNombre());
            System.out.println("Temporada\tPunts\tAssistències\tTaps\tRebots");
            System.out.println("==================================================");
            for ( EstadisticasEntity e : players) {
                System.out.println(e.getTemporada() + "\t\t" +
                        e.getPuntosPorPartido() + " \t\t" +
                        e.getAsistenciasPorPartido() + "\t\t\t" +
                        e.getTaponesPorPartido() + " \t " +
                        e.getRebotesPorPartido());
            }
            System.out.println("==================================================");
            System.out.println("Num de registres: " + players.size());
            System.out.println("==================================================");
        }
    }

    /**
     * This method adds to EstadisticasEntity another statistics row for player id=123
     * @param session Session
     * @param temporada String
     * @param puntos Double
     * @param rebotes Double
     */
    public static void insertPlayer123Statistics(Session session, String temporada, Double puntos, Double rebotes) {
        session.beginTransaction();

        // Create new Estadisticas
        EstadisticasEntity est = new EstadisticasEntity();
        est.setId(obtenirMaxID() + 1);
        est.setTemporada(temporada);
        est.setJugadoresByJugador(session.load(JugadoresEntity.class, 123));
        est.setPuntosPorPartido(puntos);
        est.setAsistenciasPorPartido(0.0);
        est.setTaponesPorPartido(0.0);
        est.setRebotesPorPartido(rebotes);

        // Save insert
        session.save(est);

        //Commit the transaction
        session.getTransaction().commit();
        System.out.println("Successfully inserted a new row in EstadisticasEntity");
    }

    /**
     * This method gets the highest PK on EstadisticasEntity
     * @return int
     */
    public static int obtenirMaxID() {
        Session session = getSession();
        Query q = session.createQuery("select max(e.id) from EstadisticasEntity as e");
        Object res = q.uniqueResult();
        int max = (int)res;
        session.close();
        return max;
    }

    /**
     * This method gathers the maximum scorer per season
     * @param session Session
     */
    public static void maxScorerPerSeason(Session session) {
        // Native query with Session
        SQLQuery query = session.createSQLQuery(
                """
                        select e.temporada, max(e.puntos_por_partido)
                        from estadisticas e, jugadores j
                        where e.jugador = j.codigo
                        group by e.temporada
                        order by e.temporada;""");
        //Query 1 result list
        List<Object[]> seasonMax = query.list();

        System.out.println("Millors puntuadors per temporada");
        for (Object[] season : seasonMax) {
            SQLQuery query2 = session.createSQLQuery("select codigo, nombre, nombre_equipo\n" +
                    "from jugadores j, estadisticas e\n" +
                    "where puntos_por_partido = " + Double.parseDouble(season[1].toString()) + " and j.codigo = e.jugador\n" +
                    "group by nombre_equipo, nombre, codigo;");
            //Query 2 result list
            List<Object[]> playerInfo = query2.list();
            Object[] player = playerInfo.get(0);

            System.out.println("===============================");
            System.out.println("Temporada: " + season[0]);
            System.out.printf("%d, %s, %s\n",
                    Integer.parseInt(player[0].toString()),
                    player[1],
                    player[2]);
            System.out.println("Punts: " + season[1]);

        }
    }
}
