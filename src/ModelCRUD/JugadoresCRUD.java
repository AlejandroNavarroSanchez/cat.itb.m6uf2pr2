package ModelCRUD;

import Model.JugadoresEntity;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;
import java.util.List;

public class JugadoresCRUD {
    /**
     * This method interacts with the database to update a player's weight
     * @param session Session
     * @param cod int
     * @param weight int
     */
    public static void updateWeight(Session session, int cod, Integer weight) {
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaUpdate<JugadoresEntity> criteriaUpdate = cb.createCriteriaUpdate(JugadoresEntity.class);
        Root<JugadoresEntity> root = criteriaUpdate.from(JugadoresEntity.class);
        criteriaUpdate.set("peso", weight);
        criteriaUpdate.where(cb.equal(root.get("codigo"), cod));

        Transaction transaction = session.beginTransaction();
        session.createQuery(criteriaUpdate).executeUpdate();
        transaction.commit();
        System.out.println("Successfully updated row in JugadoresEntity where cod is " + cod);
    }

    /**
     * This method calculates the average points per player.
     * @param session Session
     */
    public static void showAvgPointsPerPlayer(Session session) {
        // Native query with Session
        SQLQuery query = session.createSQLQuery("""
                select j.nombre_equipo, e.jugador, j.nombre, avg(e.puntos_por_partido)
                from estadisticas e, jugadores j
                where e.jugador = j.codigo
                group by e.jugador, j.nombre, j.nombre_equipo
                order by 1;""");

        SQLQuery query2 = session.createSQLQuery("""
                select nombre_equipo
                from jugadoresgroup by nombre_equipo;""");

        List<Object> teams = query2.list();
        List<Object[]> rows = query.list();
        Object[] row = rows.get(0);

        System.out.println("Número d'Equips: " + teams.size());
        System.out.println("===========================");
        System.out.println("Equip: " + row[0].toString());
        for (int i = 1; i <= rows.size(); i++) {
            row = rows.get(i-1);

            String team = row[0].toString();
            String nextTeam = null;
            if (i < rows.size() - 1)
                 nextTeam = rows.get(i)[0].toString();
            int cod = Integer.parseInt(row[1].toString());
            String name = row[2].toString();
            Double avgPoints = Double.parseDouble(row[3].toString());

            System.out.printf("%d, %s: %.2f\n", cod, name, avgPoints);
            if (nextTeam != null && !nextTeam.equals(team)) {
                System.out.println("===========================");
                System.out.println("Equip: " + nextTeam);
            }

        }
    }
}
